export default {
  port: 5050,
  dbUri: "mongodb://localhost:27017/swagger-express-mongoose-node-mongo",
  saltWorkFactor: 10,
  accessTokenTtl: "15m",
  refreshTokenTtl: "1y",
  accessTokenPrivateKey: `ACCESS_TOKEN_PRIVATE_KEY`,
  accessTokenPublicKey: `ACCESS_TOKEN_PUBLIC_KEY`,
  refreshTokenPrivateKey: `REFRESH_PRIVATE_KEY`,
  refreshTokenPublicKey: `REFRESH_PUBLIC_KEY`,
};
