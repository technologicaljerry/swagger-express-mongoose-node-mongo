import { object, string, TypeOf } from "zod";

/**
 * @openapi
 * components:
 *  schemas:
 *    CreateUserInput:
 *      type: object
 *      required:
 *        - userName
 *        - firstName
 *        - lastName
 *        - email
 *        - password
 *        - passwordConfirmation
 *      properties:
 *        userName:
 *          type: string
 *          default: technojerry
 *        firstName:
 *          type: string
 *          default: Techno
 *        lastName:
 *          type: string
 *          default: Jerry
 *        email:
 *          type: string
 *          default: jerry@technojerry.com
 *        password:
 *          type: string
 *          default: Jerry@123
 *        passwordConfirmation:
 *          type: string
 *          default: Jerry@123
 *    CreateUserResponse:
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *        userName:
 *          type: string
 *        _id:
 *          type: string
 *        createdAt:
 *          type: string
 *        updatedAt:
 *          type: string
 */

export const createUserSchema = object({
  body: object({
    userName: string({
      required_error: "User Name is required",
    }),
    firstName: string({
      required_error: "First Name is required",
    }),
    lastName: string({
      required_error: "Last Name is required",
    }),
    email: string({
      required_error: "Email is required",
    }).email("Not a valid email"),
    password: string({
      required_error: "Password is required",
    }).min(6, "Password too short - should be 6 chars minimum"),
    passwordConfirmation: string({
      required_error: "passwordConfirmation is required",
    }),
  }).refine((data) => data.password === data.passwordConfirmation, {
    message: "Passwords do not match",
    path: ["passwordConfirmation"],
  }),
});

export type CreateUserInput = Omit<
  TypeOf<typeof createUserSchema>,
  "body.passwordConfirmation"
>;
